package rosello.josep.ibm_gnb.injection.component

import dagger.Component
import rosello.josep.ibm_gnb.ui.main.MainTransactionViewModel
import rosello.josep.ibm_gnb.injection.module.NetworkModule
import rosello.josep.ibm_gnb.ui.main.MainViewModel
import javax.inject.Singleton

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {

    /**
     * Injects required dependencies into the specified ViewModels.
     */
    fun inject(ViewModel: MainTransactionViewModel)
    fun inject(ViewModel: MainViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}
