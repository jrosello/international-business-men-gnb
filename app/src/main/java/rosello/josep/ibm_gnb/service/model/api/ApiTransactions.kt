package rosello.josep.ibm_gnb.service.model.api

import com.google.gson.annotations.SerializedName

data class ApiTransactions(

	@field:SerializedName("amount")
	var amount: String? = null,

	@field:SerializedName("currency")
	var currency: String? = null,

	@field:SerializedName("sku")
	var sku: String? = null,

    @SerializedName("id") val id: Long
)