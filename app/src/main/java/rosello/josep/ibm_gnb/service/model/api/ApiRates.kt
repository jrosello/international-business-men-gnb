package rosello.josep.ibm_gnb.service.model.api

data class ApiRates(
	val rate: String? = null,
	val from: String? = null,
	val to: String? = null
)
