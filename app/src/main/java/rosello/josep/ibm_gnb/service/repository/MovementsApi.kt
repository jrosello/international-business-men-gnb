package rosello.josep.ibm_gnb.service.repository

import io.reactivex.Observable
import retrofit2.http.GET
import rosello.josep.ibm_gnb.service.model.api.ApiRates
import rosello.josep.ibm_gnb.service.model.api.ApiTransactions

/**
 *  The interface which provides methods to get result of webservices
 */
interface MovementsApi {

    /**
     * Get List of rates types
     */
    @GET("rates.json")
    fun getRates(): Observable<List<ApiRates>>

    /**
     * Get list of products
     */
    @GET("transactions.json")
    fun getTransactions(): Observable<List<ApiTransactions>>
}