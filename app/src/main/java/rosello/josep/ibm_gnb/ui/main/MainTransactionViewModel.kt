package rosello.josep.ibm_gnb.ui.main

import android.arch.lifecycle.MutableLiveData
import rosello.josep.ibm_gnb.base.BaseViewModel
import rosello.josep.ibm_gnb.service.model.api.ApiTransactions
import java.math.BigDecimal
import java.math.RoundingMode

class MainTransactionViewModel : BaseViewModel() {
    private val nameSku = MutableLiveData<String>()
    private val amount = MutableLiveData<String>()

    fun bind(transactions: ApiTransactions) {
        nameSku.value = transactions.sku
        val roundedAmount = BigDecimal(transactions.amount).setScale(2, RoundingMode.HALF_EVEN)
        amount.value = roundedAmount.toString() + " " + transactions.currency
    }

    fun getNameSku(): MutableLiveData<String> {
        return nameSku
    }

    fun getAmount(): MutableLiveData<String> {
        return amount
    }
}
