package rosello.josep.ibm_gnb.ui.main

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import rosello.josep.ibm_gnb.R
import rosello.josep.ibm_gnb.databinding.MainItemTransactionBinding
import rosello.josep.ibm_gnb.service.model.api.ApiTransactions

class TransactionsListAdapter : RecyclerView.Adapter<TransactionsListAdapter.ViewHolder>() {
    private lateinit var transactionsList: List<ApiTransactions>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: MainItemTransactionBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.main_item_transaction, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        transactionsList[position].let { holder.bind(it) }
    }

    override fun getItemCount(): Int {
        if (::transactionsList.isInitialized)
            transactionsList.let { return it.size }
        return 0
    }

    fun updatePostList(apiTransactions: List<ApiTransactions>) {
        this.transactionsList = apiTransactions
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: MainItemTransactionBinding) : RecyclerView.ViewHolder(binding.root) {
        private val viewModel = MainTransactionViewModel()

        fun bind(apiTransactions: ApiTransactions) {
            viewModel.bind(apiTransactions)
            binding.viewModel = viewModel
        }
    }
}