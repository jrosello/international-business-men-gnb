package rosello.josep.ibm_gnb.ui.main

import android.arch.lifecycle.MutableLiveData
import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import rosello.josep.ibm_gnb.R
import rosello.josep.ibm_gnb.base.BaseViewModel
import rosello.josep.ibm_gnb.service.model.api.ApiRates
import rosello.josep.ibm_gnb.service.model.api.ApiTransactions
import rosello.josep.ibm_gnb.service.repository.MovementsApi
import javax.inject.Inject

class MainViewModel : BaseViewModel() {
    //Api services
    @Inject
    lateinit var movementsApi: MovementsApi
    //Adapter
    val transactionsListAdapter: TransactionsListAdapter = TransactionsListAdapter()
    private val transactionsList: ArrayList<ApiTransactions> = arrayListOf()
    private lateinit var originalTransactionsList: List<ApiTransactions>

    //Visibility & error tools
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadRates() }

    lateinit var ratesList: List<ApiRates>

    private lateinit var subscription: Disposable

    init {
        loadRates()
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    //API SERVICES
    private fun loadRates() {
        subscription = movementsApi.getRates()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrieveStart() }
            .subscribe(
                { result -> onRetrieveRatesSuccess(result) },
                { onRetrievePostListError() }
            )
    }

    private fun loadTransactions() {
        subscription = movementsApi.getTransactions()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnTerminate { onRetrieveFinish() }
            .subscribe(
                { result -> onRetrieveTransactionsSuccess(result) },
                { onRetrievePostListError() }
            )
    }

    //NOTIFY UI
    private fun onRetrieveStart() {
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrieveFinish() {
        loadingVisibility.value = View.GONE
    }

    private fun onRetrieveRatesSuccess(apiRatesList: List<ApiRates>) {
        this.ratesList = apiRatesList
        loadTransactions()
    }

    private fun onRetrieveTransactionsSuccess(transactionsList: List<ApiTransactions>) {
        this.originalTransactionsList = transactionsList
        transactionsListAdapter.updatePostList(getTransactionsWithSameCurrency(transactionsList, "EUR")) //INITIAL VALUE
    }

    private fun onRetrievePostListError() {
        loadingVisibility.value = View.GONE
        errorMessage.value = R.string.generic_error_message
    }

    //Business Logic

    fun updateTransactionsCurrency(currency: String) {
        for (transaction in transactionsList) {
            transaction.amount = (getAmountByCurrency(transaction, currency)).toString()
            transaction.currency = currency
        }
        transactionsListAdapter.updatePostList(transactionsList)

    }

    /**
     * Return List with all transactions grouped with same currency
     */
    private fun getTransactionsWithSameCurrency(
        originTransactionsList: List<ApiTransactions>, currency: String
    ): List<ApiTransactions> {
        //Check values
        originTransactionsList.let {
            ratesList.let {
                //Check for every Transaction and group by productName equalizing the amount to same currency
                for (a in originTransactionsList) {
                    val index = transactionsList.indexOfFirst { it.sku == a.sku } // -1 if not found
                    if (index == -1) {
                        a.amount = getAmountByCurrency(a, currency).toString()
                        a.currency = currency
                        transactionsList.add(a)
                    } else {
                        transactionsList[index].amount?.let {
                            transactionsList[index].amount =
                                (it.toFloat().plus(getAmountByCurrency(a, currency))).toString()
                        }
                    }
                }
            }
            return transactionsList
        }
    }

    /**
     * Return Amount to currency.
     * Otherwise return 0
     */
    private fun getAmountByCurrency(
        originTransaction: ApiTransactions,
        currency: String
    ): Float {
        originTransaction.amount?.let {
            if (originTransaction.currency.equals(currency)) {
                return it.toFloat()
            } else {
                val rateCurrency = originTransaction.currency?.let { it1 ->
                    getIndexRates(it1, currency)
                }
                rateCurrency.let { fl ->
                    originTransaction.amount?.let { originalAmount ->
                        val amountConverted = fl?.times(originalAmount.toFloat())
                        amountConverted?.let { amount ->
                            return amount
                        }
                    }
                }
            }
        }
        //Cannot parse currency, Don't Add. Maybe show warning?
        return 0f
    }

    /**
     * Find index rate item equals to currencyOrigin
     */
    private fun getIndexRatesFrom(currencyOrigin: String): String {
        val index = ratesList.indexOfFirst {
            it.from == currencyOrigin
        }
        if (index != -1)
            ratesList[index].to?.let {
                return it
            }
        return ""
    }

    /**
     * Return rate for currency
     */
    private fun getIndexRates(currencyOrigin: String, currencyType: String): Float? {
        var index = ratesList.indexOfFirst {
            it.from == currencyOrigin && it.to == currencyType
        }
        var rateValue = 1f
        while (index == -1) {
            getIndexRates(getIndexRatesFrom(currencyOrigin), currencyType)?.let {
                index = it.toInt()
                rateValue *= it
                //IMPROVE: Add new badge into list
            }
        }
        return ratesList[index].rate?.toFloat()?.times(rateValue)
    }

}
