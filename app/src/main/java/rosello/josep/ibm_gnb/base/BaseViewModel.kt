package rosello.josep.ibm_gnb.base


import android.arch.lifecycle.ViewModel
import rosello.josep.ibm_gnb.injection.component.DaggerViewModelInjector
import rosello.josep.ibm_gnb.injection.component.ViewModelInjector
import rosello.josep.ibm_gnb.injection.module.NetworkModule
import rosello.josep.ibm_gnb.ui.main.MainTransactionViewModel
import rosello.josep.ibm_gnb.ui.main.MainViewModel

abstract class BaseViewModel:ViewModel(){
    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is MainTransactionViewModel -> injector.inject(this)
            is MainViewModel -> injector.inject(this)
        }
    }
}